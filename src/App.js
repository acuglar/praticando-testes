import { useState } from 'react';

import './App.css';
import Display from './components/Display';
import Input from './components/Input';

function App() {
  const [name, setName] = useState('');
  const [nameList, setNameList] = useState([]);

  const handleClick = () => {
    setNameList([...nameList, name]);
    setName('')
  };

  const handleChange = event => {
    setName(event.target.value)
  }
  return (
    <div className="App">
      <header className="App-header">
        <ul>
          <Display list={nameList} />
        </ul>
        <Input name={name} handleChange={handleChange} handleClick={handleClick} />
      </header>
    </div>
  );
}

export default App;
