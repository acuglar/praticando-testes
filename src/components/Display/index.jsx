const Display = ({ list }) => {
  return (
    <>
      {list.map((name, idx) => (
        <li key={idx}>{name}</li>
      ))}
    </>
  );
};

export default Display;
