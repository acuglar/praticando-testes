const Input = ({ name, handleChange, handleClick }) => {
  return (
    <>
      <input value={name} onChange={handleChange} />
      <button onClick={handleClick} disabled={!name}>
        Add
      </button>
    </>
  );
};

export default Input;
