import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';

describe("when everything is ok", () => {

  test("Should name appear on the list when the user type a value on input and click on the button", async () => {

    render(<App />)
    userEvent.type(screen.getByRole("textbox"), "Filipe")
    userEvent.click(screen.getByRole("button"))

    const myList = await screen.findAllByRole("listitem")

    expect(myList).toHaveLength(1)
  })
  test("Should not let user be able to click on the button when the input value us empty", () => {

    render(<App />)
    expect(screen.getByRole("button")).toBeDisabled();
  })
})